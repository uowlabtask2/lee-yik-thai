#include <iostream>

using namespace std;

int main()
{
   float fah,cel;

        cout << "Convert Celsius to Fahrenheit" << endl;
        cout << "Please enter Celsius : ";
        cin>>cel;
        while ( cin.fail() )
        {       
                cin.clear();
                cin.ignore(); 
                cout<<"Invaild input, please insert again"<<endl;
                cin>>cel;
                       
        }
        fah = (cel*1.8)+32;
        cout << "Fahrenheit : " << fah << endl;
        cout << "--------------------------------------" <<endl;

    return 0;
}